Feature: Prueba Netflix
  Como un usuario
  Quiero Loguearme en la aplicación de Netflix
  Para ver la lista de las películas

  @TagLoginNetflix @LoginNetflix_HP
  Scenario Outline: Login - Iniciar sesión a la aplicacion Netflix con credenciales correctas
    Given que un usuario ingresa a la web de Netflix
    And clickea en el boton Iniciar
    When ingresa sus credenciales "<sUser>"
    And ingresa su contraseña "<sPassword>"
    And clickea en iniciar sesion
    Then Inicia sesion con exito y muestra el home de la aplicacion
    Examples:
      | sUser                   | sPassword   |
      | naira.qa.test@gmail.com | Testing2023 |

  @TagLoginNetflix @LoginNetflix_Fail
  Scenario Outline: Login - Iniciar sesión a la aplicacion de Netflix con usuario no registrado
    Given que un usuario ingresa a la web de Netflix
    And clickea en el boton Iniciar
    When ingresa sus credenciales "<sUser>"
    And ingresa su contraseña "<sPassword>"
    And clickea en iniciar sesion
    Then valida el mensaje de error "<sMensajeError>" en la aplicacion
    Examples:
      | sUser                     | sPassword | sMensajeError |
      | naira.challenge@gmail.com | Test2023  | Reinténtalo   |

  @TagLoginNetflix @LoginNetflix_Fail2
  Scenario Outline: Login - Iniciar sesión a la aplicacion de Netflix con password incorrecto
    Given que un usuario ingresa a la web de Netflix
    And clickea en el boton Iniciar
    When ingresa sus credenciales "<sUser>"
    And ingresa su contraseña "<sPassword>"
    And clickea en iniciar sesion
    Then valida el mensaje de error "<sMensajeError>" en la aplicacion
    Examples:
      | sUser                   | sPassword | sMensajeError |
      | naira.qa.test@gmail.com | Test2023  | Reinténtalo   |

