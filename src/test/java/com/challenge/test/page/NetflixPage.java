package com.challenge.test.page;

import com.challenge.framework.generic.WebDriverDOM;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;


@DefaultUrl("https://www.netflix.com/pe/")
public class NetflixPage extends WebDriverDOM {
    public static boolean _valuePage;
    @FindBy(xpath = "//a[@data-uia='header-login-link']")
    private WebElementFacade btnEmpezar;
    @FindBy(id = "id_userLoginId")
    private WebElementFacade txtEmail;
    @FindBy(id = "id_password")
    private WebElementFacade txtPassword;
    @FindBy(xpath = "//button[@data-uia='login-submit-button']")
    private WebElementFacade btnInicioSesion;
    @FindBy(className = "our-story-card-title")
    private WebElementFacade txtMensajeHome;
    @FindBy(className = "ui-message-contents")
    private WebElementFacade txtMensajeError;

    /**
     * Valida el titulo de la pagina encontrada
     *
     * @param paginaEsperada valor del titulo esperado
     * @return true     cuando el titulo de la pagina contiene el texto buscado
     */
    public boolean validarPagina(String paginaEsperada) throws Throwable {
        String paginaEncontrada = getDriver().getTitle();
        if (paginaEncontrada.contains(paginaEsperada)) {
            System.out.println("Se encontro la pagina : " + paginaEncontrada);
            return _valuePage = true;
        } else {
            System.out.println("No se encontro la pagina : " + paginaEsperada);
            return _valuePage = false;
        }
    }

    /**
     * Realiza clic en el boton del elemento
     */
    public void clicEmpezar() throws Throwable {
        waitElementVisible(btnEmpezar, 5);
        if (_valuePage == true) {
            if (isElementClickable(btnEmpezar)) {
                btnEmpezar.click();
            }
        }
    }

    /**
     * Ingresa el valor del email al campo
     *
     * @param sEmail valor del email
     */
    public void ingresarEmail(String sEmail) throws Throwable {
        waitElementVisible(txtEmail, 5);
        if (validarPagina("Netflix") == true) {
            if (isElementPresent(txtEmail)) {
                txtEmail.sendKeys(sEmail);
            }
        }
    }

    /**
     * Ingresa el valor del password al campo
     *
     * @param sPassword valor del password
     */
    public void ingresarPassword(String sPassword) throws Throwable {
        waitElementVisible(txtPassword, 10);
        if (validarPagina("Netflix") == true) {
            if (isElementPresent(txtPassword)) {
                txtPassword.sendKeys(sPassword);
            }
        }
    }

    public void clicIniciarSesion() throws Throwable {
        waitElementVisible(btnInicioSesion, 5);
        if (isElementClickable(btnInicioSesion)) {
            btnInicioSesion.click();
        }
    }

    /**
     * Valida el mensaje de bienvenida
     *
     * @return true     cuando el mensaje de bienvenida cotiene el esperado
     */
    public boolean validarMensajeBienvenida() {
        waitElementVisible(txtMensajeHome, 5);
        System.out.println("El texto encontrado es :  "+txtMensajeHome.getText());
        return txtMensajeHome.getText().contains("Películas");
    }

    /**
     * Valida el mensaje de error
     *
     * @param sMensajeError valor del mensaje de error esperado
     * @return true     cuando el mensaje de error cotiene el esperado
     */
    public boolean validarMensajeError(String sMensajeError) {
        waitElementVisible(txtMensajeError, 5);
        System.out.println("El mensaje Capturado es: "+txtMensajeError.getText());
        return txtMensajeError.getText().contains(sMensajeError);
    }

    /**
     * Cierra el Navegador
     */
    public void closePage() {
        getDriver().close();
    }
}
