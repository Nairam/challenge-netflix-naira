package com.challenge.test.runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty", "html:Reporte/Reporte de prueba.html"},
        features = {"src/test/resources/features"},
        glue = {"com/challenge/test/stepdefinition"},
        tags = "@LoginNetflix"
)
public class TestRunnerTest {

}
