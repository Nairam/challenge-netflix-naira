package com.challenge.test.step;

import com.challenge.framework.lib.WebDriverManager;
import com.challenge.test.page.NetflixPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class NetflixStep extends ScenarioSteps {

    NetflixPage netflixPage = new NetflixPage();

    @Step
    public void que_un_usuario_ingresa_a_la_web_de_netflix() throws Throwable {
        netflixPage.setDriver(WebDriverManager.setWebDriver("chrome"));
        netflixPage.open();
        netflixPage.getDriver().manage().window().maximize();
        System.out.println("Titulo : " + netflixPage.getDriver().getTitle());
        System.out.println("URL : " + netflixPage.getDriver().getCurrentUrl());
        netflixPage.validarPagina("Netflix");
    }

    public void clickeaEnElBotonIniciar() throws Throwable {
        netflixPage.clicEmpezar();
    }

    public void ingresa_sus_credenciales(String sUser) throws Throwable {
        netflixPage.ingresarEmail(sUser);
    }

    public void ingresa_su_contraseña(String sPassword) throws Throwable {
        netflixPage.ingresarPassword(sPassword);
    }

    public void clickea_en_iniciar_sesion() throws Throwable {
        netflixPage.clicIniciarSesion();
    }

    public void inicia_sesion_con_exito_y_muestra_el_home_de_la_aplicacion() throws Throwable {
        Assert.assertTrue(netflixPage.validarMensajeBienvenida());
       // netflixPage.closePage();
    }

    public void valida_el_mensaje_de_error_en_la_aplicacion(String sMensajeError) throws Throwable {
        Assert.assertTrue(netflixPage.validarMensajeError(sMensajeError));
        netflixPage.closePage();
    }

}


