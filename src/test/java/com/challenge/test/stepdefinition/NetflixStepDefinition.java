package com.challenge.test.stepdefinition;

import com.challenge.test.step.NetflixStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NetflixStepDefinition {

    @Steps
    NetflixStep netflixStep = new NetflixStep();

    @Given("^que un usuario ingresa a la web de Netflix$")
    public void queUnUsuarioIngresaALaWebDeNetflix() throws Throwable {
        System.out.println("****************** Bienvenidos a la prueba challenge - Netflix ******************");
        netflixStep.que_un_usuario_ingresa_a_la_web_de_netflix();
    }

    @And("^clickea en el boton Iniciar$")
    public void clickeaEnElBotonIniciar() throws Throwable {
        netflixStep.clickeaEnElBotonIniciar();
    }

    @When("^ingresa sus credenciales \"([^\"]*)\"$")
    public void ingresaSusCredenciales(String sUser) throws Throwable {
        netflixStep.ingresa_sus_credenciales(sUser);
    }

    @And("^ingresa su contraseña \"([^\"]*)\"$")
    public void ingresaSuContraseña(String sPassword) throws Throwable {
        netflixStep.ingresa_su_contraseña(sPassword);
    }

    @And("^clickea en iniciar sesion$")
    public void clickeaEnIniciarSesion() throws Throwable {
        netflixStep.clickea_en_iniciar_sesion();
    }

    @Then("^Inicia sesion con exito y muestra el home de la aplicacion$")
    public void iniciaSesionConExitoYMuestraElHomeDeLaAplicacion() throws Throwable {
        netflixStep.inicia_sesion_con_exito_y_muestra_el_home_de_la_aplicacion();
    }

    @Then("^valida el mensaje de error \"([^\"]*)\" en la aplicacion$")
    public void validaElMensajeDeErrorEnLaAplicacion(String sMensajeError) throws Throwable {
        netflixStep.valida_el_mensaje_de_error_en_la_aplicacion(sMensajeError);
    }

}
