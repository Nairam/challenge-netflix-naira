package com.challenge.framework.config;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    public static void PopulateSettings() throws IOException {
        ConfigReader reader = new ConfigReader();
        reader.ReadProperty();
    }

    private void ReadProperty() throws IOException {

        Properties p = new Properties();
        String environment = System.getProperty("environment");
        String propertiesFile = "ConfigIntegration.properties";

        if (environment != null) {
            propertiesFile = "Config" + StringUtils.capitalize(environment) + ".properties";
        }
        p.load(getClass().getClassLoader().getResourceAsStream(propertiesFile));
        Settings.URLbase = p.getProperty("URLbase");
        Settings.URLambiente = p.getProperty("URLambiente");
        Settings.AUTNeoWeb = p.getProperty("AUTNeoWeb");

    }

}
